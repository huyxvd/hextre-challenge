﻿using Application.ViewModels.ChemicalsViewModels;
using FluentValidation;

namespace WebAPI.Validations
{
    public class CreateChemicalValidation : AbstractValidator<CreateChemical>
    {
        public CreateChemicalValidation()
        {
            RuleFor(x => x.Name).NotEmpty().MaximumLength(100);
        }
    }
}
