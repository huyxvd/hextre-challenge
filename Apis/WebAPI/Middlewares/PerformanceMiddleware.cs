﻿using System.Diagnostics;

namespace WebAPI.Middlewares
{
    public class PerformanceMiddleware : IMiddleware
    {
        private readonly Stopwatch stopwatch;

        public PerformanceMiddleware(Stopwatch stopwatch)
        {
            this.stopwatch = stopwatch;
        }
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            var stopwatch = Stopwatch.StartNew(); // Bắt đầu đo thời gian

            await next(context); // Tiếp tục xử lý request

            stopwatch.Stop(); // Dừng đo khi request hoàn tất

            // Ghi log thời gian xử lý (học sinh có thể thay thế bằng một hệ thống log chuyên nghiệp)
            Console.WriteLine($"Request to {context.Request.Path} took {stopwatch.ElapsedMilliseconds} ms");
        }
    }
}
