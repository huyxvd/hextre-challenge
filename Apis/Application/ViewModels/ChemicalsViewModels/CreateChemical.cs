﻿namespace Application.ViewModels.ChemicalsViewModels
{
    public class CreateChemical
    {
        public string ChemicalType { get; set; }

        public int PreHarvestIntervalInDays { get; set; }

        public string ActiveIngredient { get; set; }

        public string Name { get; set; }
    }
}
