﻿namespace Application.Commons
{
    /// <summary>
    /// Lớp hỗ trợ phân trang (pagination) cho dữ liệu dạng danh sách.
    /// </summary>
    public class Pagination<T>
    {
        /// <summary>
        /// Tổng số phần tử trong danh sách.
        /// </summary>
        public int TotalItemsCount { get; set; }

        /// <summary>
        /// Số lượng phần tử trên mỗi trang.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Tổng số trang dựa trên tổng số phần tử và số phần tử trên mỗi trang.
        /// </summary>
        public int TotalPagesCount
        {
            get
            {
                var temp = TotalItemsCount / PageSize;
                // Nếu chia hết, lấy nguyên temp, nếu không thì cộng thêm 1 trang.
                return TotalItemsCount % PageSize == 0 ? temp : temp + 1;
            }
        }

        /// <summary>
        /// Chỉ mục của trang hiện tại (bắt đầu từ 0).
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// Kiểm tra có trang tiếp theo không.
        /// Nếu PageIndex + 1 nhỏ hơn tổng số trang, thì còn trang tiếp theo.
        /// </summary>
        public bool Next => PageIndex + 1 < TotalPagesCount;

        /// <summary>
        /// Kiểm tra có trang trước đó không.
        /// Nếu PageIndex > 0 thì có trang trước.
        /// </summary>
        public bool Previous => PageIndex > 0;

        /// <summary>
        /// Danh sách phần tử của trang hiện tại.
        /// </summary>
        public ICollection<T> Items { get; set; }
    }
}
