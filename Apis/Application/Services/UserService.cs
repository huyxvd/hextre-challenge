﻿using Application;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels.UserViewModels;
using Domain.Entities;

namespace Infrastructures.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAuthService _authService;

        public UserService(IUnitOfWork unitOfWork, IAuthService authService)
        {
            _unitOfWork = unitOfWork;
            _authService = authService;
        }

        public async Task<string> LoginAsync(UserLoginDTO userObject)
        {
            var user = await _unitOfWork.UserRepository.GetUserByUserNameAndPasswordHash(userObject.UserName, userObject.Password.Hash());
            return _authService.GenerateAuthToken(user);
        }

        public async Task RegisterAsync(UserRegisterDTO userObject)
        {
            // check username exited
            var isExited = await _unitOfWork.UserRepository.CheckUserNameExited(userObject.UserName);

            if(isExited)
            {
                throw new Exception("Username exited please try again");
            }

            var newUser = new User
            {
                UserName = userObject.UserName,
                PasswordHash = userObject.Password.Hash()
            };

            await _unitOfWork.UserRepository.AddAsync(newUser);
            await _unitOfWork.SaveChangeAsync();
        }
    }
}
