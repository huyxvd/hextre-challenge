﻿using Application;
using Application.Commons;
using Application.Repositories;
using Application.ViewModels.ChemicalsViewModels;
using Domain.Entities;
using Infrastructures.Services;
using Moq;

public class ChemicalServiceTests
{
    private readonly Mock<IUnitOfWork> _unitOfWorkMock;
    private readonly Mock<IChemicalRepository> _chemicalRepositoryMock;
    private readonly ChemicalService _chemicalService;

    public ChemicalServiceTests()
    {
        _unitOfWorkMock = new Mock<IUnitOfWork>();
        _chemicalRepositoryMock = new Mock<IChemicalRepository>();

        _unitOfWorkMock.Setup(u => u.ChemicalRepository).Returns(_chemicalRepositoryMock.Object);

        _chemicalService = new ChemicalService(_unitOfWorkMock.Object);
    }

    [Fact]
    public async Task GetChemicalAsync_ShouldReturnListOfChemicalViewModel()
    {
        // Arrange
        var chemicals = new List<Chemical>
        {
            new Chemical { Id = new Guid(), Name = "Chemical A" },
            new Chemical { Id = new Guid(), Name = "Chemical B" }
        };

        _chemicalRepositoryMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(chemicals);

        // Act
        var result = await _chemicalService.GetChemicalAsync();

        // Assert
        Assert.NotNull(result);
        Assert.Equal(2, result.Count);
        Assert.Equal("Chemical A", result[0].Name);
        Assert.Equal("Chemical B", result[1].Name);
    }

    [Fact]
    public async Task CreateChemicalAsync_ShouldReturnChemicalViewModel_WhenSaveIsSuccessful()
    {
        // Arrange
        var createChemical = new CreateChemical { Name = "New Chemical" };
        var chemical = new Chemical { Id = new Guid(), Name = "New Chemical" };

        _chemicalRepositoryMock.Setup(repo => repo.AddAsync(It.IsAny<Chemical>())).Returns(Task.CompletedTask);
        _unitOfWorkMock.Setup(u => u.SaveChangeAsync()).ReturnsAsync(1);

        // Act
        var result = await _chemicalService.CreateChemicalAsync(createChemical);

        // Assert
        Assert.NotNull(result);
        Assert.Equal("New Chemical", result.Name);
    }

    [Fact]
    public async Task CreateChemicalAsync_ShouldReturnNull_WhenSaveFails()
    {
        // Arrange
        var createChemical = new CreateChemical { Name = "New Chemical" };

        _chemicalRepositoryMock.Setup(repo => repo.AddAsync(It.IsAny<Chemical>())).Returns(Task.CompletedTask);
        _unitOfWorkMock.Setup(u => u.SaveChangeAsync()).ReturnsAsync(0);

        // Act
        var result = await _chemicalService.CreateChemicalAsync(createChemical);

        // Assert
        Assert.Null(result);
    }

    [Fact]
    public async Task GetChemicalPagingsionAsync_ShouldReturnPaginatedChemicals()
    {
        // Arrange
        int pageIndex = 0, pageSize = 2;
        var chemicals = new Pagination<Chemical>
        {
            Items = new List<Chemical>
            {
            new Chemical { Id = new Guid(), Name = "Chemical A" },
            new Chemical { Id = new Guid(), Name = "Chemical B" }
            },
            PageIndex = pageIndex,
            PageSize = pageSize,
            TotalItemsCount = 5
        };

        _chemicalRepositoryMock.Setup(repo => repo.ToPagination(pageIndex, pageSize)).ReturnsAsync(chemicals);

        // Act
        var result = await _chemicalService.GetChemicalPagingsionAsync(pageIndex, pageSize);

        // Assert
        Assert.NotNull(result);
        Assert.Equal(2, result.Items.Count);
        Assert.Equal(5, result.TotalItemsCount);
    }
}
