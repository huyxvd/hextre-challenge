﻿namespace Application.Commons
{
    // Lớp đại diện cho cấu hình Database trong appsettings.json
    public class Database
    {
        /// <summary>
        /// Chuỗi kết nối đến cơ sở dữ liệu.
        /// Ví dụ: "Server=myServer;Database=myDB;User Id=myUser;Password=myPassword;"
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Cờ kiểm tra xem có sử dụng In-Memory Database hay không.
        /// Nếu true -> Sử dụng In-Memory DB (thường dùng để test).
        /// Nếu false -> Kết nối đến Database thực tế.
        /// </summary>
        public bool IsMemoryDB { get; set; }
    }

    // Lớp đại diện cho cấu hình JWT trong appsettings.json
    public class JWT
    {
        /// <summary>
        /// Khóa bí mật dùng để mã hóa và xác thực JWT Token.
        /// Lưu ý: Phải có độ dài tối thiểu 128 bit (~16 ký tự).
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// Thời gian tồn tại của JWT Token (tính bằng phút).
        /// Ví dụ: 60 -> Token sẽ hết hạn sau 1 giờ.
        /// </summary>
        public int ExpireTimeInMinutes { get; set; }

        /// <summary>
        /// Cờ xác định có kiểm tra thời gian hết hạn của Token hay không.
        /// Nếu true -> Token hết hạn sẽ không được chấp nhận.
        /// Nếu false -> Token hết hạn vẫn có thể sử dụng (KHÔNG KHUYẾN KHÍCH).
        /// </summary>
        public bool ValidateLifetime { get; set; }

        /// <summary>
        /// Nếu backend của bạn chạy trên domain yourapi.com, đặt "https://yourapi.com".
        /// Nếu đang chạy local, có thể dùng "https://localhost:5001" (hoặc port API của bạn).
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// Nếu frontend là web chạy trên yourfrontend.com, đặt "https://yourfrontend.com".
        /// Nếu client là mobile app hoặc một ứng dụng khác, đặt tên phù hợp như "yourmobileapp" hoặc "yourclientid".
        /// </summary>
        public string Audience { get; set; }
    }

    // Lớp chứa toàn bộ cấu hình của ứng dụng (tích hợp Database & JWT)
    public class AppConfiguration
    {
        /// <summary>
        /// Cấu hình Database, bao gồm ConnectionString & IsMemoryDB.
        /// </summary>
        public Database Database { get; set; }

        /// <summary>
        /// Cấu hình JWT, bao gồm SecretKey, thời gian hết hạn, ValidateLifetime.
        /// </summary>
        public JWT JWT { get; set; }
    }
}
