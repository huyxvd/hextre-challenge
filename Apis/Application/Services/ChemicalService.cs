﻿using Application;
using Application.Commons;
using Application.Interfaces;
using Application.MappingObjects;
using Application.ViewModels.ChemicalsViewModels;

namespace Infrastructures.Services
{
    public class ChemicalService : IChemicalService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ChemicalService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<List<ChemicalViewModel>> GetChemicalAsync()
        {
            var chemicals = await _unitOfWork.ChemicalRepository.GetAllAsync();
            var result = chemicals.Select(item => item.ToChemicalViewModel()).ToList();
            return result;
        }

        public async Task<ChemicalViewModel?> CreateChemicalAsync(CreateChemical chemical)
        {
            var chemicalObj = chemical.CreateChemicalViewModelToChemical();
            await _unitOfWork.ChemicalRepository.AddAsync(chemicalObj);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
            if (isSuccess)
            {
                return chemicalObj.ToChemicalViewModel();
            }
            return null;
        }

        public async Task<Pagination<ChemicalViewModel>> GetChemicalPagingsionAsync(int pageIndex = 0, int pageSize = 10)
        {
            var chemicals = await _unitOfWork.ChemicalRepository.ToPagination(pageIndex, pageSize);
            Pagination<ChemicalViewModel> result = new Pagination<ChemicalViewModel>()
            {
                Items = chemicals.Items.Select(item => item.ToChemicalViewModel()).ToList(),
                PageIndex = pageIndex,
                PageSize = pageSize,
                TotalItemsCount = chemicals.TotalItemsCount
            };
            return result;
        }
    }
}
