﻿using Application.ViewModels.ChemicalsViewModels;
using Domain.Entities;

namespace Application.MappingObjects
{
    public static class CreateChemicalViewModelExtension
    {
        public static Chemical CreateChemicalViewModelToChemical(this CreateChemical viewModel)
        {
            return new Chemical
            {
                ActiveIngredient = viewModel.ActiveIngredient,
                ChemicalType = viewModel.ChemicalType,
                PreHarvestIntervalInDays = viewModel.PreHarvestIntervalInDays,
                Name = viewModel.Name,
            };
        }
    }
}
