﻿using Application.Commons;
using Application.Interfaces;
using Domain.Entities;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Infrastructures
{
    /// <summary>
    /// Dịch vụ xác thực, chịu trách nhiệm tạo JWT token cho người dùng.
    /// </summary>
    public class AuthService : IAuthService
    {
        private readonly AppConfiguration _config;
        private readonly ICurrentTime _currentTime;

        /// <summary>
        /// Khởi tạo AuthService với cấu hình ứng dụng và thời gian hiện tại.
        /// </summary>
        public AuthService(AppConfiguration config, ICurrentTime currentTime)
        {
            _config = config;
            _currentTime = currentTime;
        }

        /// <summary>
        /// Tạo JWT token cho người dùng dựa trên thông tin đăng nhập.
        /// </summary>
        /// <param name="user">Thông tin người dùng</param>
        /// <returns>Chuỗi JWT token</returns>
        public string GenerateAuthToken(User user)
        {
            // 1. Mã hóa secret key từ cấu hình thành dạng bytes
            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.JWT.SecretKey));

            // 2. Tạo thông tin ký tên cho token (HMAC SHA256)
            var signingCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

            // 3. Danh sách claim (dữ liệu đính kèm trong token)
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.UserName) // Lưu tên người dùng trong token
            };

            // 4. Tạo đối tượng JWT với thời gian hết hạn
            var jwtToken = new JwtSecurityToken(
                issuer: _config.JWT.Issuer, // Người phát hành token (ví dụ: "yourdomain.com")
                audience: _config.JWT.Audience, // Đối tượng có thể sử dụng token này
                claims: claims,
                expires: _currentTime.GetCurrentTime().AddMinutes(_config.JWT.ExpireTimeInMinutes), // Thời gian hết hạn
                signingCredentials: signingCredentials // Chữ ký bảo mật
            );

            // 5. Chuyển JWT token thành chuỗi và trả về
            return new JwtSecurityTokenHandler().WriteToken(jwtToken);
        }
    }
}
