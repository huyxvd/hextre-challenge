﻿namespace WebAPI.Middlewares
{
    public class GlobalExceptionMiddleware : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                // Ghi log lỗi để kiểm tra sau (học sinh có thể tìm hiểu thêm về cách log chuyên nghiệp với Serilog, NLog, v.v.)
                Console.WriteLine(ex.ToString());

                // Trả về phản hồi lỗi chung cho client thay vì lộ thông tin lỗi chi tiết.
                context.Response.StatusCode = 500;
                context.Response.ContentType = "text/plain";
                await context.Response.WriteAsync($"Server has encountered an exception: {ex.Message}");
            }
        }
    }
}
