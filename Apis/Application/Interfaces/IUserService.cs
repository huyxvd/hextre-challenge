﻿using Application.ViewModels.UserViewModels;

namespace Application.Interfaces
{
    public interface IUserService
    {
        public Task RegisterAsync(UserRegisterDTO userObject);
        public Task<string> LoginAsync(UserLoginDTO userObject);
    }
}
