﻿using Application;
using Application.Commons;
using Application.Interfaces;
using Application.Repositories;
using AutoFixture;
using Infrastructures;
using Microsoft.EntityFrameworkCore;
using Microsoft.Identity.Client;
using Moq;

namespace Domain.Tests
{
    public class SetupTest : IDisposable
    {
        protected readonly Fixture _fixture;
        protected readonly Mock<IUnitOfWork> _unitOfWorkMock;
        protected readonly Mock<IChemicalService> _chemicalServiceMock;
        protected readonly Mock<ICurrentUserService> _claimsServiceMock;
        protected readonly Mock<ICurrentTime> _currentTimeMock;
        protected readonly Mock<IChemicalRepository> _chemicalRepositoryMock;
        protected readonly Mock<IUserRepository> _userRepository;
        protected readonly AppDbContext _dbContext;
        protected readonly AppConfiguration _appConfiguration;

        public SetupTest()
        {
            
            _fixture = new Fixture();
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _chemicalServiceMock = new Mock<IChemicalService>();
            _claimsServiceMock = new Mock<ICurrentUserService>();
            _currentTimeMock = new Mock<ICurrentTime>();
            _chemicalRepositoryMock = new Mock<IChemicalRepository>();
            _userRepository = new Mock<IUserRepository>();

            var options = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            _dbContext = new AppDbContext(options);

            _currentTimeMock.Setup(x => x.GetCurrentTime()).Returns(DateTime.UtcNow);
            _claimsServiceMock.Setup(x => x.GetCurrentUserId).Returns(Guid.Empty);

            _appConfiguration = new AppConfiguration
            {
                Database = new Database
                {
                    ConnectionString = "Server=localhost;Database=MyAppDb;User Id=sa;Password=yourpassword;",
                    IsMemoryDB = true
                },
                JWT = new JWT
                {
                    SecretKey = "MySuperSecretKey123456",
                    ExpireTimeInMinutes = 60,
                    ValidateLifetime = true,
                    Issuer = "https://myapi.com",
                    Audience = "https://myfrontend.com"
                }
            };

        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
