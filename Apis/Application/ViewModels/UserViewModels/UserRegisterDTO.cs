﻿namespace Application.ViewModels.UserViewModels
{
    public class UserRegisterDTO
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
