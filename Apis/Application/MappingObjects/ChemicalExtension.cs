﻿using Application.ViewModels.ChemicalsViewModels;
using Domain.Entities;

namespace Application.MappingObjects
{
    public static class ChemicalExtension
    {
        public static ChemicalViewModel ToChemicalViewModel(this Chemical chemical) {
            return new ChemicalViewModel
            {
                _Id = chemical.Id.ToString(),
                ActiveIngredient = chemical.ActiveIngredient,
                ChemicalType = chemical.ChemicalType,
                CreationDate = chemical.CreationDate,
                DeletionDate = chemical.DeletionDate,
                ModificationDate = chemical.ModificationDate,
                Name = chemical.Name,
                PreHarvestIntervalInDays = chemical.PreHarvestIntervalInDays,
            };
        }
    }
}
