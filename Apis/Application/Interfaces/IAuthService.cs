﻿using Domain.Entities;

namespace Application.Interfaces
{
    // Interface này chịu trách nhiệm tạo JWT token cho user.
    // Nó thuộc về tầng Application vì xác thực là một phần của logic nghiệp vụ ứng dụng,
    // nhưng không thuộc về domain thuần túy.
    public interface IAuthService
    {
        // Phương thức này nhận một đối tượng User và tạo ra chuỗi JWT token.
        string GenerateAuthToken(User user);
    }
}
