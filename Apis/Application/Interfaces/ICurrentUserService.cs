﻿namespace Application.Interfaces
{
    public interface ICurrentUserService
    {
        public Guid GetCurrentUserId { get; }
    }
}
