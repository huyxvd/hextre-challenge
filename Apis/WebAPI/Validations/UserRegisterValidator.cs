﻿using Application.ViewModels.UserViewModels;
using FluentValidation;

namespace WebAPI.Validations
{
    public class UserRegisterValidator : AbstractValidator<UserRegisterDTO>
    {
        public UserRegisterValidator()
        {
            RuleFor(x => x.UserName)
                .NotEmpty().WithMessage("Tên đăng nhập không được để trống.")
                .MinimumLength(5).WithMessage("Tên đăng nhập phải có ít nhất 5 ký tự.")
                .MaximumLength(20).WithMessage("Tên đăng nhập không được quá 20 ký tự.");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Mật khẩu không được để trống.")
                .MinimumLength(8).WithMessage("Mật khẩu phải có ít nhất 8 ký tự.")
                .Matches(@"[A-Z]").WithMessage("Mật khẩu phải chứa ít nhất một chữ cái viết hoa.")
                .Matches(@"[a-z]").WithMessage("Mật khẩu phải chứa ít nhất một chữ cái thường.")
                .Matches(@"\d").WithMessage("Mật khẩu phải chứa ít nhất một số.")
                .Matches(@"[\W_]").WithMessage("Mật khẩu phải chứa ít nhất một ký tự đặc biệt.");
        }
    }
}
