﻿using Application;
using Application.Commons;
using Application.Interfaces;
using Application.Repositories;
using Application.Services;
using Infrastructures.Repositories;
using Infrastructures.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructures
{
    public static class DenpendencyInjection
    {
        public static IServiceCollection AddInfrastructuresService(this IServiceCollection services, AppConfiguration configuration)
        {
            services.AddScoped<IChemicalService, ChemicalService>();
            services.AddScoped<IChemicalRepository, ChemicalRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserService, UserService>();
            services.AddSingleton<ICurrentTime, CurrentTime>();
            services.AddSingleton<IAuthService, AuthService>();

            if (configuration.Database.IsMemoryDB)
            {
                // this configuration just use in-memory for fast develop

                services.AddDbContext<AppDbContext>(option => option.UseInMemoryDatabase("test"));
            }
            else
            {
                // ATTENTION: if you do migration please check file README.md
                services.AddDbContext<AppDbContext>(option => option.UseSqlServer(configuration.Database.ConnectionString));
            }

            return services;
        }
    }
}
